#ifdef IOT_CONFIG_USE_X509_CERT
  #define IOT_CONFIG_DEVICE_CERT "Device Certificate"
  #define IOT_CONFIG_DEVICE_CERT_PRIVATE_KEY "Device Certificate Private Key"
#endif // IOT_CONFIG_USE_X509_CERT


#define IOT_CONFIG_IOTHUB_FQDN "Nasz-iot-hub-1.azure-devices.net"
#define IOT_CONFIG_DEVICE_ID "esp32"
#define IOT_CONFIG_DEVICE_KEY "a1mohXc2DPMbC/M5RKcoAmts1Gi1QlxBiRY9CuoOuOA="

// Use device key if not using certificates
#ifndef IOT_CONFIG_USE_X509_CERT
#endif // IOT_CONFIG_USE_X509_CERT

// Publish 1 message every 2 seconds
#define TELEMETRY_FREQUENCY_MILLISECS 8000
