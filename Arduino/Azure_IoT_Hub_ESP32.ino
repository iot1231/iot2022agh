#include <cstdlib>
#include <string.h>
#include <time.h>

#include <WiFi.h>
#include <mqtt_client.h>
#include <Adafruit_Sensor.h>
#include <Adafruit_BME280.h>
#include "SPIFFS.h"
#include "FS.h"

// Azure IoT SDK for C includes
#include <az_core.h>
#include <az_iot.h>
#include <azure_ca.h>

// Additional headers
#include "AzIoTSasToken.h"
#include "SerialLogger.h"
#include "iot_configs.h"

#define AZURE_SDK_CLIENT_USER_AGENT "c%2F" AZ_SDK_VERSION_STRING "(ard;esp32)"

// Utility macros and defines
#define sizeofarray(a) (sizeof(a) / sizeof(a[0]))
#define NTP_SERVERS "pool.ntp.org", "time.nist.gov"
#define MQTT_QOS1 1
#define DO_NOT_RETAIN_MSG 0
#define SAS_TOKEN_DURATION_IN_MINUTES 60
#define UNIX_TIME_NOV_13_2017 1510592825

#define PST_TIME_ZONE -8
#define PST_TIME_ZONE_DAYLIGHT_SAVINGS_DIFF 1

#define GMT_OFFSET_SECS (PST_TIME_ZONE * 3600)
#define GMT_OFFSET_SECS_DST ((PST_TIME_ZONE + PST_TIME_ZONE_DAYLIGHT_SAVINGS_DIFF) * 3600)

// Translate iot_configs.h defines into variables used by the sample
//static const char* ssid = IOT_CONFIG_WIFI_SSID;
//static const char* password = IOT_CONFIG_WIFI_PASSWORD;
static const char* host = IOT_CONFIG_IOTHUB_FQDN;
static const char* mqtt_broker_uri = "mqtts://" IOT_CONFIG_IOTHUB_FQDN;
static const char* device_id = IOT_CONFIG_DEVICE_ID;
static const int mqtt_port = 8883;

// Memory allocated for the sample's variables and structures.
static esp_mqtt_client_handle_t mqtt_client;
static az_iot_hub_client client;

static char mqtt_client_id[128];
static char mqtt_username[128];
static char mqtt_password[200];
static uint8_t sas_signature_buffer[256];
static unsigned long next_telemetry_send_time_ms = 0;
static char telemetry_topic[128];
static uint8_t telemetry_payload[100];
static uint32_t telemetry_send_count = 0;

#define INCOMING_DATA_BUFFER_SIZE 128
static char incoming_data[INCOMING_DATA_BUFFER_SIZE];

// Auxiliary functions
#ifndef IOT_CONFIG_USE_X509_CERT
static AzIoTSasToken sasToken(
    &client,
    AZ_SPAN_FROM_STR(IOT_CONFIG_DEVICE_KEY),
    AZ_SPAN_FROM_BUFFER(sas_signature_buffer),
    AZ_SPAN_FROM_BUFFER(mqtt_password));
#endif // IOT_CONFIG_USE_X509_CERT

Adafruit_BME280 bme;
bool status;
float temperature, humidity, pressure, altitude;

const char* AP_ssid = "AP ESP 32";
const char* AP_pass = "12345678";

String ssid;
String password;
String header;
String body;
String UNIQUE_ID;

bool bodyActive = false;
bool requestPOST = false;

WiFiServer server(80);

static void initializeTime()
{
  Logger.Info("Setting time using SNTP");

  configTime(GMT_OFFSET_SECS, GMT_OFFSET_SECS_DST, NTP_SERVERS);
  time_t now = time(NULL);
  while (now < UNIX_TIME_NOV_13_2017)
  {
    delay(500);
    Serial.print(".");
    now = time(nullptr);
  }
  Serial.println("");
  Logger.Info("Time initialized!");
}

void receivedCallback(char* topic, byte* payload, unsigned int length)
{
  Logger.Info("Received [");
  Logger.Info(topic);
  Logger.Info("]: ");
  for (int i = 0; i < length; i++)
  {
    Serial.print((char)payload[i]);
  }
  Serial.println("");
}

static void initializeIoTHubClient()
{
  az_iot_hub_client_options options = az_iot_hub_client_options_default();
  options.user_agent = AZ_SPAN_FROM_STR(AZURE_SDK_CLIENT_USER_AGENT);

  if (az_result_failed(az_iot_hub_client_init(
          &client,
          az_span_create((uint8_t*)host, strlen(host)),
          az_span_create((uint8_t*)device_id, strlen(device_id)),
          &options)))
  {
    Logger.Error("Failed initializing Azure IoT Hub client");
    return;
  }

  size_t client_id_length;
  if (az_result_failed(az_iot_hub_client_get_client_id(
          &client, mqtt_client_id, sizeof(mqtt_client_id) - 1, &client_id_length)))
  {
    Logger.Error("Failed getting client id");
    return;
  }

  if (az_result_failed(az_iot_hub_client_get_user_name(
          &client, mqtt_username, sizeofarray(mqtt_username), NULL)))
  {
    Logger.Error("Failed to get MQTT clientId, return code");
    return;
  }

  Logger.Info("Client ID: " + String(mqtt_client_id));
  Logger.Info("Username: " + String(mqtt_username));
}

static int initializeMqttClient()
{
#ifndef IOT_CONFIG_USE_X509_CERT
  if (sasToken.Generate(SAS_TOKEN_DURATION_IN_MINUTES) != 0)
  {
    Logger.Error("Failed generating SAS token");
    return 1;
  }
#endif

  esp_mqtt_client_config_t mqtt_config;
  memset(&mqtt_config, 0, sizeof(mqtt_config));
  mqtt_config.uri = mqtt_broker_uri;
  mqtt_config.port = mqtt_port;
  mqtt_config.client_id = mqtt_client_id;
  mqtt_config.username = mqtt_username;

#ifdef IOT_CONFIG_USE_X509_CERT
  Logger.Info("MQTT client using X509 Certificate authentication");
  mqtt_config.client_cert_pem = IOT_CONFIG_DEVICE_CERT;
  mqtt_config.client_key_pem = IOT_CONFIG_DEVICE_CERT_PRIVATE_KEY;
#else // Using SAS key
  mqtt_config.password = (const char*)az_span_ptr(sasToken.Get());
#endif

  mqtt_config.keepalive = 30;
  mqtt_config.disable_clean_session = 0;
  mqtt_config.disable_auto_reconnect = false;
  mqtt_config.event_handle = mqtt_event_handler;
  mqtt_config.user_context = NULL;
  mqtt_config.cert_pem = (const char*)ca_pem;

  mqtt_client = esp_mqtt_client_init(&mqtt_config);

  if (mqtt_client == NULL)
  {
    Logger.Error("Failed creating mqtt client");
    return 1;
  }

  esp_err_t start_result = esp_mqtt_client_start(mqtt_client);

  if (start_result != ESP_OK)
  {
    Logger.Error("Could not start mqtt client; error code:" + start_result);
    return 1;
  }
  else
  {
    Logger.Info("MQTT client started");
    return 0;
  }
}

/*
 * @brief           Gets the number of seconds since UNIX epoch until now.
 * @return uint32_t Number of seconds.
 */
static uint32_t getEpochTimeInSecs() { return (uint32_t)time(NULL); }


static void connectToWiFi()
{
  Logger.Info("Connecting to WIFI SSID " + String(ssid));

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid.c_str(), password.c_str());
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");

  Logger.Info("WiFi connected, IP address: " + WiFi.localIP().toString());
}

static esp_err_t mqtt_event_handler(esp_mqtt_event_handle_t event)
{
  switch (event->event_id)
  {
    int i, r;

    case MQTT_EVENT_ERROR:
      Logger.Info("MQTT event MQTT_EVENT_ERROR");
      break;
    case MQTT_EVENT_CONNECTED:
      Logger.Info("MQTT event MQTT_EVENT_CONNECTED");

      Serial.print("Subskrybcja: ");
      Serial.println(AZ_IOT_HUB_CLIENT_C2D_SUBSCRIBE_TOPIC);
      Serial.print("Telemetria: ");
      Serial.println(telemetry_topic);
      Serial.print("Port: ");
      Serial.println(mqtt_port);
      Serial.print("Haslo: ");
      Serial.println(mqtt_password);

      r = esp_mqtt_client_subscribe(mqtt_client, AZ_IOT_HUB_CLIENT_C2D_SUBSCRIBE_TOPIC, 1);
      if (r == -1)
      {
        Logger.Error("Could not subscribe for cloud-to-device messages.");
      }
      else
      {
        Logger.Info("Subscribed for cloud-to-device messages; message id:" + String(r));
      }

      break;
    case MQTT_EVENT_DISCONNECTED:
      Logger.Info("MQTT event MQTT_EVENT_DISCONNECTED");
      break;
    case MQTT_EVENT_SUBSCRIBED:
      Logger.Info("MQTT event MQTT_EVENT_SUBSCRIBED");
      break;
    case MQTT_EVENT_UNSUBSCRIBED:
      Logger.Info("MQTT event MQTT_EVENT_UNSUBSCRIBED");
      break;
    case MQTT_EVENT_PUBLISHED:
      Logger.Info("MQTT event MQTT_EVENT_PUBLISHED");
      break;
    case MQTT_EVENT_DATA:
      Logger.Info("MQTT event MQTT_EVENT_DATA");

      for (i = 0; i < (INCOMING_DATA_BUFFER_SIZE - 1) && i < event->topic_len; i++)
      {
        incoming_data[i] = event->topic[i];
      }
      incoming_data[i] = '\0';
      Logger.Info("Topic: " + String(incoming_data));

      for (i = 0; i < (INCOMING_DATA_BUFFER_SIZE - 1) && i < event->data_len; i++)
      {
        incoming_data[i] = event->data[i];
      }
      incoming_data[i] = '\0';
      Logger.Info("Data: " + String(incoming_data));

      break;
    case MQTT_EVENT_BEFORE_CONNECT:
      Logger.Info("MQTT event MQTT_EVENT_BEFORE_CONNECT");
      break;
    default:
      Logger.Error("MQTT event UNKNOWN");
      break;
  }

  return ESP_OK;
}

static void getTelemetryPayload(az_span payload, az_span* out_payload)
{
  az_span original_payload = payload;

  payload = az_span_copy(payload, AZ_SPAN_FROM_STR("{ \"msgCount\": "));
  (void)az_span_u32toa(payload, telemetry_send_count++, &payload);
  payload = az_span_copy(payload, AZ_SPAN_FROM_STR(" }"));
  payload = az_span_copy_u8(payload, '\0');

  *out_payload = az_span_slice(
      original_payload, 0, az_span_size(original_payload) - az_span_size(payload) - 1);
}

static void sendTelemetry()
{
  az_span telemetry = AZ_SPAN_FROM_BUFFER(telemetry_payload);
  String odczyty[5];
  odczyty[0] = String(bme.readTemperature());
  String daneDoWyslania = String("{\"Temperature\":\"")+odczyty[0]+String("\",\"mac\":\"")+UNIQUE_ID+String("\"}");
  
  Serial.print("Telemetria do wysłania: ");
  Serial.println(daneDoWyslania);

  Logger.Info("Sending telemetry ...");

  if (az_result_failed(az_iot_hub_client_telemetry_get_publish_topic(
          &client, NULL, telemetry_topic, sizeof(telemetry_topic), NULL)))
  {
    Logger.Error("Failed az_iot_hub_client_telemetry_get_publish_topic");
    return;
  }

  getTelemetryPayload(telemetry, &telemetry);
  
  Logger.Info("Board ID: " + UNIQUE_ID);

  if (esp_mqtt_client_publish(
          mqtt_client,
//          "devices/esp32/messages/events/",
          telemetry_topic,
//          (const char*)az_span_ptr(telemetry),
          daneDoWyslania.c_str(),
          strlen(daneDoWyslania.c_str()),
//          az_span_size(telemetry),
          MQTT_QOS1,
          DO_NOT_RETAIN_MSG)
      == 0)
  {
    Logger.Error("Failed publishing");
  }
  else
  {
    Logger.Info("Message published successfully");
  }
}

static void establishConnection()
{
  UNIQUE_ID = String("esp31") + String(";") + String(ESP.getEfuseMac());
  if(!CheckNetworkData()){
    Serial.println("Brak danych do sieci");
    SetAccessPoint();
  }
  else{
    ConnectToWiFi();
    initializeTime();
    initializeIoTHubClient();
    (void)initializeMqttClient();
    status = bme.begin(0x76);
  }  
}

void setup() {
  establishConnection();
}

void loop()
{
  if(WiFi.status() != WL_CONNECTED){
    if(!CheckNetworkData()){
      WaitForData();
    }
     else{
      ConnectToWiFi();
    }
  }

#ifndef IOT_CONFIG_USE_X509_CERT
  else if (sasToken.IsExpired())
  {
    Logger.Info("SAS token expired; reconnecting with a new one.");
    (void)esp_mqtt_client_destroy(mqtt_client);
    initializeMqttClient();
  }
#endif
  else if (millis() > next_telemetry_send_time_ms)
  {
    sendTelemetry();
    next_telemetry_send_time_ms = millis() + TELEMETRY_FREQUENCY_MILLISECS;
  }

  delay(1000);
}


bool CheckNetworkData(){
  Serial.println("Sprawdzam dane sieciowe: ");
  
  if(!SPIFFS.begin(true)){
    Serial.println("Błąd SPIFFS");
    return false;
  }

  File file_read = SPIFFS.open("/daneDoSieci.txt", "r");
 
  if (!file_read) {
    Serial.println("Błąd otwarcia pliku z danymi do sieci.");
    file_read.close();
    return false;
  }
 
  while (file_read.available()) {
    String daneOdczytane = file_read.readString();
    
    if(daneOdczytane.indexOf("gsid:") == -1 || daneOdczytane.indexOf("pass:") == -1){
      file_read.close();
      return false;
    }
    else{
      int indexPass = daneOdczytane.indexOf("pass:");
      
      ssid = daneOdczytane.substring(5, indexPass-1);
      password = daneOdczytane.substring(indexPass+5, daneOdczytane.length());
    }    
  }
 
  file_read.close();
  return true;
}

void SetAccessPoint(){
  Serial.print("Setting AP (Access Point)…");
  WiFi.softAP(AP_ssid, AP_pass);
  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  
  server.begin();
}

void WaitForData(){
  WiFiClient client = server.available();
  Serial.println("Klient");
  if (client) {
    Serial.println("New Client.");
    String currentLine = "";
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        header += c;
        if (c == '\n') {
          if (currentLine.indexOf('"') != -1)
            body = currentLine;
          if (currentLine.indexOf("POST") != -1)
            requestPOST = true;

          if (currentLine.length() == 0) {
            if(bodyActive || !requestPOST){
              if(CheckNetworkCredentials(body)){
                SaveNetworkData();
                Serial.println("Dane dostępowe poprawne.");
                client.println("HTTP/1.1 200 OK");
                client.println("Content-type:text/html");
                client.println("Connection: close");
                client.println();
                client.println(UNIQUE_ID.c_str());
              }
              else{
                Serial.println("Dane dostępowe niepoprawne.");
                client.println("HTTP/1.1 400 Bad Request");
                client.println("Content-type:text/html");
                client.println("Connection: close");
                client.println();
                client.println("");
              }
              
              client.println();
              bodyActive = false;
              break;
            }

            bodyActive = true;       
            
          } else {
            currentLine = "";
          }
        } else if (c != '\r') {
          currentLine += c;
        }
      }
    }
    header = "";
    body = "";
    
//    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
    bodyActive = false;
    requestPOST = false;
//    server.close();
  }  
  else{
    delay(1000);
  }
}

void SaveNetworkData(){
  File file_write = SPIFFS.open("/daneDoSieci.txt", "w");
  if(!file_write){
    Serial.println("Błąd otwarcia pliku z danymi do sieci");
    return;
  }

  if(file_write.print("gsid:"+ssid+";pass:"+password)){
    Serial.println("Tekst dodany");
  }
  else{
    Serial.println("Błąd dodania tekstu");
  }

  file_write.close();
}

bool CheckNetworkCredentials(String dane){
  int maxNumberOfTries = 10;
  int commaIndex = dane.indexOf(',');
  String login;
  String pass;

  login = dane.substring(1, commaIndex-1);
  pass = dane.substring(commaIndex+2, dane.length()-1);

  Serial.println("Sprawdzam dane sieci: ");
  WiFi.mode(WIFI_AP_STA);
  WiFi.begin(login.c_str(), pass.c_str()); 
  while (WiFi.status() != WL_CONNECTED && maxNumberOfTries > 0)
  {
    maxNumberOfTries -= 1;
    delay(500);
    Serial.print(".");
  }

  if(maxNumberOfTries > 0){
    Serial.println("Połączenie możliwe.");
    ssid = login;
    password = pass;
    return true;
  }

  Serial.println("Błąd połączenia z WiFi.");
  return false;  
}

void ConnectToWiFi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid.c_str(), password.c_str());
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print(".");
  }

  Serial.println("Połączono z WiFi");
}
