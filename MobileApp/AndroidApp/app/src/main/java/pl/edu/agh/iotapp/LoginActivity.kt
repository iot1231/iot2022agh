package pl.edu.agh.iotapp

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_homepage.*
import kotlinx.android.synthetic.main.activity_login.*
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.UnknownHostException
import java.security.MessageDigest

class LoginActivity : AppCompatActivity() {
    val LOGIN_URL: String = "https://hello-world-iot9.azurewebsites.net/api/user?"
    val REGISTER_URL: String = "https://hello-world-iot9.azurewebsites.net/api/register?code=nQXaSSEDvHv8zoRjYCrAUgLV3veWX_xCDAxz7g1mCsOLAzFuCu66ww=="

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        button_login.setOnClickListener {
            if (input_login.text.isEmpty() || input_password.text.isEmpty()) {
                textView_logLogin.text = "Wypełnij wszystkie pola przed próbą logowania"
                return@setOnClickListener
            }
            postToServer(true, LOGIN_URL)
        }

        button_register.setOnClickListener {
            if (input_login.text.isEmpty() || input_password.text.isEmpty()) {
                textView_logLogin.text = "Wypełnij wszystkie pola przed próbą rejestracji"
                return@setOnClickListener
            }
            postToServer(false, REGISTER_URL)
        }
    }


    @SuppressLint("SetTextI18n")
    fun postToServer(is_logging_in: Boolean, url_link: String) {
        // Disable buttons while doing request
        setButtons(false)

        // Separate thread for HTTP POST request
        val thread = Thread {
            log("Wysyłam dane...")

            try {
                val url = URL(url_link)
                val con = url.openConnection() as HttpURLConnection
                con.doInput = true
                con.doOutput = true
                con.requestMethod = "POST"
                con.setRequestProperty("Content-Type", "application/json");

                // Put login data in payload
                val out = OutputStreamWriter(con.getOutputStream());
                out.write("{\"Login\":\"${input_login.text}\", \"Password\":\"${hashSHA256(input_password.text.toString())}\"}");
                out.close()

                // Handle completed request
                if (con.responseCode == 200) {
                    if (!is_logging_in) {
                        // Log in inside of new thread
                        log("Zarejestrowano pomyślnie")
                        Thread.sleep(500)
                        postToServer(true, LOGIN_URL)
                        Thread.currentThread().interrupt()
                        return@Thread
                    } else {
                        log("Zalogowano pomyślnie")

                        // Read ID and list of devices from request
                        val data = con.inputStream.bufferedReader().readText()
                        var user_token = ""
                        var user_devices = arrayOf<String>()

                        if (data[data.length - 1] == ';') {
                            // Last character is ';' - no devices
                            user_token = data.slice(IntRange(0, data.length - 2))
                        } else {
                            // Extract devices' IDs
                            val dataSplit = data.split(";")
                            user_token = dataSplit[0]
                            user_devices = dataSplit[1].slice(IntRange(0, dataSplit[1].length - 2)).split(",").toTypedArray()
                        }

                        // Go to main screen
                        val intent = Intent(this, HomepageActivity::class.java)
                        intent.putExtra("user_token", user_token)
                        intent.putExtra("user_devices", user_devices)
                        startActivity(intent)

                        // Clear input and feedback
                        Thread.sleep(1000)
                        runOnUiThread {
                            input_login.text.clear()
                            input_login.clearFocus()
                            input_password.text.clear()
                            input_password.clearFocus()
                        }
                        log("")
                    }
                } else {
                    // Show appropriate error
                    if (is_logging_in) log("Dane logowania są niepoprawne\n${con.responseMessage}")
                    else log("Konto o tym loginie już istnieje\n${con.responseMessage}")
                }

                setButtons(true)
            } catch (uhe: UnknownHostException) {
                log("Brak połączenia z internetem")
                setButtons(true)
            } catch (e: Exception) {
                log("Wystąpił problem; spróbuj ponownie\n${e}")
                setButtons(true)
            }
        }
        thread.start()
    }

    fun hashSHA256(input: String): String {
        val bytes = input.toByteArray()
        val md = MessageDigest.getInstance("SHA-256")
        val digest = md.digest(bytes)
        return digest.fold("", { str, it -> str + "%02x".format(it) })
    }

    fun setButtons(enabled: Boolean) {
        runOnUiThread {
            button_login.isClickable = enabled
            button_login.isEnabled = enabled
            button_register.isClickable = enabled
            button_register.isEnabled = enabled
        }
    }

    private fun log(message: String) {
        runOnUiThread { textView_logLogin.text = message }
    }

    private fun logAppend(message: String) {
        log(textView_logLogin.text.toString() + "\n" + message)
    }
}