package pl.edu.agh.iotapp

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_device.view.*

class DeviceAdapter(
    private var devices: MutableList<DeviceClass>,
    private val parentContext: HomepageActivity
) : RecyclerView.Adapter<DeviceAdapter.DeviceViewHolder>() {
    class DeviceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeviceViewHolder {
        return DeviceViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_device,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DeviceViewHolder, position: Int) {
        var d = devices[position]
        holder.itemView.apply {
            textView_deviceIdItem.text = d.device_id
            textView_dateItem.text = d.timestamp
            textView_valueItem.text = d.temperature.toString()

            button_removeItem.setOnClickListener {
                parentContext.removeItem(d.device_id)
            }
        }
    }

    override fun getItemCount(): Int {
        return devices.size
    }


    fun removeDevice(id: String) {
        devices.removeIf { d -> d.device_id == id }
        notifyDataSetChanged()
    }

    fun updateList(data: MutableList<DeviceClass>) {
        devices = data
        notifyDataSetChanged()
    }
}