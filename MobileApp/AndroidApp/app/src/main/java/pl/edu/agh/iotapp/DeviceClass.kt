package pl.edu.agh.iotapp

data class DeviceClass(
    val device_id: String,

    var temperature: String = "--.--",
    var timestamp: String = "- brak pomiaru -"
)
