package pl.edu.agh.iotapp

import android.Manifest.permission.*
import android.content.Context
import android.location.LocationManager
import android.net.*
import android.net.wifi.WifiConfiguration
import android.net.wifi.WifiManager
import android.net.wifi.WifiNetworkSpecifier
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_homepage.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_pairing.*
import pub.devrel.easypermissions.AfterPermissionGranted
import pub.devrel.easypermissions.EasyPermissions
import java.io.InterruptedIOException
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.UnknownHostException


class PairingActivity : AppCompatActivity() {
    private var wifi_ssid = ""
    private var wifi_password = ""
    private var device_id = ""
    private val device_server_ip = "http://192.168.4.1"
    private val device_wifi_ssid = "AP ESP 32"
    private val device_wifi_password = "12345678"
    private val perms = listOf(
        INTERNET,
        ACCESS_WIFI_STATE,
        ACCESS_NETWORK_STATE,
        CHANGE_WIFI_STATE,
        CHANGE_NETWORK_STATE,
        ACCESS_FINE_LOCATION,
        ACCESS_COARSE_LOCATION
    )

    private val PAIRING_URL = "https://hello-world-iot9.azurewebsites.net/api/AddDevice?code=lgMIbIzATkU5pQRbnDKyDRX3h1CEfIDPstFnnPRRTc-2AzFunz5AYA=="

    var MAX_ATTEMPTS_1 = 12         // 1 - łączenie z siecią płytki
    val TIMEOUT_1: Long = 5000      // 2 - łączenie z siecią domową
    var MAX_ATTEMPTS_2 = 180        // 3 - wysyłanie posta na serwer
    val TIMEOUT_2: Long = 1000
    val TIMEOUT_3: Long = 5000

    private lateinit var user_token: String

    val t1_getDeviceID = Thread {
        try {
            // Wait for connection
            while (MAX_ATTEMPTS_1 > 0) {
                if (getSSID() == "\"" + device_wifi_ssid + "\"") break

                log("Sprawdzam połączenie...\nPozostałych prób: ${--MAX_ATTEMPTS_1}")
                Thread.sleep(TIMEOUT_1)
            }

            // Exit if we didn't succeed
            if (getSSID() != "\"" + device_wifi_ssid + "\"") {
                log("Nie udało się połączyć z urządzeniem.\nPowróć do poprzedniego ekranu i spróbuj ponownie.")
                releaseNetworkRequest()
                Thread.currentThread().interrupt()
                return@Thread
            }

            val url = URL(device_server_ip)
            val con = url.openConnection() as HttpURLConnection
            con.doInput = true
            con.doOutput = true
            con.requestMethod = "POST"
            con.setRequestProperty("Content-Type", "application/text");

            log("Wysyłam dane sieci WiFi ${wifi_ssid} na urządzenie...")

            // Write network data
            val out = OutputStreamWriter(con.getOutputStream());
            out.write("\"${wifi_ssid}\",\"${wifi_password}\"\n\n");
            out.close()

            if (con.responseCode == 200) {
                // Read device's ID
                device_id = con.inputStream.bufferedReader().readText()
                device_id = device_id.slice(IntRange(0, device_id.length-5))

                // Reconnect to the home network
                log("Wracam do sieci domowej...")
                releaseNetworkRequest()

                // Wait for connection
                while (MAX_ATTEMPTS_2 > 0) {
                    if (getSSID() == "\"" + wifi_ssid + "\"") break

                    log("Wracam do sieci domowej...\nPozostałych prób: ${--MAX_ATTEMPTS_2}")
                    Thread.sleep(TIMEOUT_2)
                }

                // Exit it we didn't connect
                if (getSSID() != "\"" + wifi_ssid + "\"") {
                    log("Nie udało się powrócić do sieci domowej.")
                    Thread.currentThread().interrupt()
                    return@Thread
                }

                log("")
                t2_sendDeviceIDToServer.start()
            } else if (con.responseCode == 400) {
                log(
                    "Urządzenie nie może połączyć się z podaną siecią" + "${con.responseCode} ${con.responseMessage}"
                )
                releaseNetworkRequest()
            } else if (con.responseCode == 401) {
                log(
                    "Urządzenie połączyło się z siecią, ale nie ma dostępu do serwera" + "${con.responseCode} ${con.responseMessage}"
                )
                releaseNetworkRequest()
            } else {
                log(
                    "Nie udało połączyć się z urządzeniem: " + "${con.responseCode} ${con.responseMessage}"
                )
                releaseNetworkRequest()
            }
        } catch (ie: InterruptedException) {
            // Interrupted - user can only go back
        } catch (ioe: InterruptedIOException) {
            // Interrupted - user can only go back
        } catch (e: Exception) {
            log("Zwrócono wyjątek podczas kroku 1.:\n${e}")
            releaseNetworkRequest()
        }
    }

    val t2_sendDeviceIDToServer = Thread {
        while (true) {
            try {
                Thread.sleep(TIMEOUT_3)
                log("Informuję serwer o nowym urządzeniu...")

                val url = URL(PAIRING_URL)
                val con = url.openConnection() as HttpURLConnection
                con.doInput = false
                con.doOutput = true
                con.requestMethod = "POST"
                con.setRequestProperty("Content-Type", "application/json");

                // Write device data
                val out = OutputStreamWriter(con.getOutputStream());
                out.write("{\"authToken\":\"${user_token}\", \"deviceId\":\"${device_id}\"}");
                out.close()

                if (con.responseCode == 200) {
                    // Inform about success and go back to the main screen
                    log("Pomyślnie dodano urządzenie!")
                    Thread.sleep(3000)
                    finish()
                    Thread.currentThread().interrupt()
                    return@Thread
                } else if (con.responseCode == 400) {
                    log("Urządzenie jest już powiązane z twoim kontem!")
                    Thread.currentThread().interrupt()
                    return@Thread
                } else {
                    log(
                        "Nie udało połączyć się z serwerem: " +
                                "${con.responseCode} ${con.responseMessage}"
                    )
                    Thread.currentThread().interrupt()
                    return@Thread
                }
            } catch (ie: UnknownHostException) {
                // No signal
                if (getSSID() == device_wifi_ssid) {
                    // Phone still connected with device
                    log("Telefon nadal jest połączony z siecią urządzenia. Powróć do sieci domowej.")
                } else {
                    log("Telefon nie może połączyć się z serwerem.")
                }
            } catch (ie: InterruptedException) {
                // Interrupted - user can only go back
            } catch (ioe: InterruptedIOException) {
                // Interrupted - user can only go back
            } catch (e: Exception) {
                log("Zwrócono wyjątek podczas kroku 2.:\n${e}")
                Thread.currentThread().interrupt()
                return@Thread
            }
        }
    }

    private val mNetworkCallback = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            val connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            connectivityManager.bindProcessToNetwork(network)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pairing)

        // Ask for localization permission
        requestPermissions()

        user_token = intent.getStringExtra("user_token")!!

        // Initialize pairing
        button_confirmPairing.setOnClickListener {
            // Exit if we don't have permission - can't change network without it
            if (!arePermissionsGranted()) {
                log("Aplikacja nie posiada wymaganych uprawnień.\nPrzyznaj uprawnienia i spróbuj ponownie.")
                return@setOnClickListener
            }

            // Exit if there isn't GPS enabled - can't get network's name without it
            if (!isGPSOn()) {
                log("Aplikacja nie jest w stanie zweryfikować nazwy sieci WiFi bez włączonej lokalizacji.\nWłącz lokalizację GPS i spróbuj ponownie.")
                return@setOnClickListener
            }

            // Exit if passwprd is too short - can't change network
            if (input_passwordPairing.text.toString().length < 8) {
                log("Podane hasło do sieci jest błędne. Poprawne hasło do sieci WiFi zawiera co najmniej 8 znaków.\nPodaj poprawne hasło i spróbuj ponownie.")
                return@setOnClickListener
            }

            // Exit it phone isn't connected to the correct network - can't verify if data is correct
            if (input_passwordPairing.text.toString().length < 8) {
                log("Telefon nie jest połączony z secią WiFi, którą podajesz.\nUpewnij się że nazwa SSID jest poprawnie wpisana i spróbuj ponownie.")
                return@setOnClickListener
            }

            // Disable buttons - we can only try once
            button_confirmPairing.isClickable = false
            button_confirmPairing.isEnabled = false

            // Read network data
            wifi_ssid = input_ssidPairing.text.toString()
            wifi_password = input_passwordPairing.text.toString()

            // Connect to the other network
            log("Łączę z siecią urządzenia ${device_wifi_ssid}...")
            networkRequest(device_wifi_ssid, device_wifi_password)

            // Pairing
            t1_getDeviceID.start()
        }
    }

    // Block return while pairing
    override fun onBackPressed() {
        if (!t1_getDeviceID.isAlive && !t2_sendDeviceIDToServer.isAlive) super.onBackPressed()
    }


    private fun isGPSOn(): Boolean {
        val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun arePermissionsGranted(): Boolean {
        for (perm in perms) {
            if (!EasyPermissions.hasPermissions(this, perm)) return false
        }
        return true
    }

    @AfterPermissionGranted(1)
    fun requestPermissions() {
        // Ask user for permissions
        for (perm in perms) {
            if (!EasyPermissions.hasPermissions(this, perm)) {
                EasyPermissions.requestPermissions(
                    this,
                    "Aplikacja potrzebuje tych pozwoleń, aby połączyć się z siecią urządzenia", 1, perm
                )
            }
        }
    }

    private fun getSSID(): String {
        val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        return wifiManager.connectionInfo.ssid
    }

    private fun log(message: String) {
        runOnUiThread { textView_logPairing.text = message }
    }

    private fun logAppend(message: String) {
        log(textView_logPairing.text.toString() + "\n" + message)
    }


    private fun releaseNetworkRequest() {
        val connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        connectivityManager.unregisterNetworkCallback(mNetworkCallback)
        try {
            connectivityManager.bindProcessToNetwork(null)
        } catch (e: Exception) {
            log(e.toString())
        }
    }

    private fun networkRequest(ssid: String, password: String) {
        if (android.os.Build.VERSION.SDK_INT >= 29) {
            val specifier = WifiNetworkSpecifier.Builder()
                .setSsid(ssid)
                .setWpa2Passphrase(password)
                .build()
            val request = NetworkRequest.Builder()
                .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                .setNetworkSpecifier(specifier)
                .build()

            val connectivityManager = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            connectivityManager.requestNetwork(request, mNetworkCallback)
        } else {
            log("DETECTED SDK <29 !!!!!!!!")
            var conf = WifiConfiguration()
            conf.SSID = "\"" + ssid + "\""
            conf.preSharedKey = password
            var wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            var netid = wifiManager.addNetwork(conf)
            val bool1 = wifiManager.disconnect()
            val bool2 = wifiManager.enableNetwork(netid, true)
            val bool3 = wifiManager.reconnect()
            logAppend("NetID: ${netid}\n.disconnect():${bool1}\n.enableNetwork():${bool2}\n.reconnect():${bool3}")
        }
    }
}