package pl.edu.agh.iotapp

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_homepage.*
import kotlinx.android.synthetic.main.activity_login.*
import org.json.JSONArray
import org.json.JSONTokener
import java.io.InterruptedIOException
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL
import java.net.UnknownHostException
import java.util.*


class HomepageActivity : AppCompatActivity() {
    private lateinit var user_token: String
    private lateinit var user_devices: List<String>
    private lateinit var device_adapter: DeviceAdapter

    private val REFRESH_URL = "https://hello-world-iot9.azurewebsites.net/api/GetTelemetry?code=FM4mJ_wL0Ja29Mc4y-IHr-cwV1AniU__gh-dp3Fo53XJAzFu_NIH5Q=="
    private val TEMP_REFRESH_URL = "https://jsonblob.com/api/jsonBlob/1066786763883954176"
    private val LOGOUT_URL = "https://hello-world-iot9.azurewebsites.net/api/Logout?code=UW9B1Qm6Iy-eRQkl38hpCHqpLaxQWJoxOIojQb3BwSMbAzFuDkdcbg=="
    private val REMOVE_URL = "https://hello-world-iot9.azurewebsites.net/api/RemoveDevice?code=KAJPiCVXjuB-9bAWp1VPaDU_RpH00VGLvuOkJ8iqALchAzFumrhW_Q=="

    private val FAILURE_TIMEOUT: Long = 5000
    private val NO_CONNECTION_TIMEOUT: Long = 20000
    private val SUCCESS_TIMEOUT: Long = 60000

    @SuppressLint("SetTextI18n")
    private val loadDataRunnable: Runnable = Runnable {
        while (true) {
            try {
                setButtons(false, true)
                log("Odświeżanie pomiarów...")

                val url = URL(REFRESH_URL)
                val con = url.openConnection() as HttpURLConnection
                con.doInput = true
                con.doOutput = true
                con.requestMethod = "POST"

                // Put login data in payload
                val out = OutputStreamWriter(con.getOutputStream());
                out.write("{\"authToken\":\"${user_token}\"}");
                out.close()

                val data = con.inputStream.bufferedReader().readText()

                // Handle completed request
                if (con.responseCode == 200) {
                    // Odśwież pomiary
                    runOnUiThread {
                        device_adapter.updateList(parseDevicesJSON(data))
                    }

                    // Wait 60 seconds
                    log("")
                    setButtons(true, true)
                    Thread.sleep(SUCCESS_TIMEOUT)
                } else {
                    log("Wystąpił błąd podczas pobierania danych: ${con.responseCode}: ${con.responseMessage}")
                    setButtons(true, true)
                    Thread.sleep(FAILURE_TIMEOUT)
                }
            } catch (uhe: UnknownHostException) {
                log("Brak połączenia z internetem")
                setButtons(true, true)
                Thread.sleep(NO_CONNECTION_TIMEOUT)
            } catch (ie: InterruptedException) {
                // Interrupted - end thread
                setButtons(true, true)
                break
            } catch (ioe: InterruptedIOException) {
                // Interrupted - end thread
                setButtons(true, true)
                break
            } catch (e: Exception) {
                log("Zwrócono wyjątek:\n${e}")
                setButtons(true, true)
                Thread.sleep(FAILURE_TIMEOUT)
            }
        } // while-end
    } // loadDataRunnable-end
    var loadDataThread: Thread = Thread(loadDataRunnable)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_homepage)

        // Get user data
        user_token = intent.getStringExtra("user_token")!!
        user_devices = (intent.getStringArrayExtra("user_devices") as Array<String>).toList()

        // Initialize device list
        device_adapter = DeviceAdapter(generateInitialDeviceList(), this)
        recyclerView_devicesList.adapter = device_adapter
        recyclerView_devicesList.layoutManager = LinearLayoutManager(this)

        // Download data if user has at least 1 device
        if (user_devices.size > 0) loadDataThread.start()
        else log("Brak urządzeń!\nSparuj swoje urządzenie aby otrzymywać dane")

        button_refresh.setOnClickListener {
            // Restart data-downloading thread
            if (loadDataThread.isAlive && !loadDataThread.isInterrupted) loadDataThread.interrupt()
            loadDataThread = Thread(loadDataRunnable)
            loadDataThread.start()
        }

        button_pair.setOnClickListener {
            // Go to device pairing screen
            val intent = Intent(this, PairingActivity::class.java)
            intent.putExtra("user_token", user_token)
            startActivity(intent)
        }

        button_logout.setOnClickListener {
            logOut()
        }
    }

    override fun onBackPressed() {
        val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(a)
    }

    fun generateInitialDeviceList(): MutableList<DeviceClass> {
        val output = mutableListOf<DeviceClass>()
        for (device_id in user_devices) output.add(
            DeviceClass(
                device_id = device_id,
                timestamp = "Pobieram pomiary..."
            )
        )
        return output
    }

    fun parseDevicesJSON(json: String): MutableList<DeviceClass> {
        val jsonArray = JSONTokener(json).nextValue() as JSONArray
        val output = mutableListOf<DeviceClass>()

        for (i in 0 until jsonArray.length()) {
            output.add(
                DeviceClass(
                    jsonArray.getJSONObject(i).getString("device_id"),
                    jsonArray.getJSONObject(i).getString("temperature"),
                    jsonArray.getJSONObject(i).getString("timestamp")
                )
            )
        }

        output.sortBy { it.device_id }
        return output
    }

    fun decodeBase64(input: String): String {
        val decoder = Base64.getDecoder()
        return String(decoder.decode(input))
    }

    fun removeItem(device_id: String) {
        Thread {
            try {
                setButtons(false)
                log("Usuwam urządzenie ${device_id}...")
                val url = URL(REMOVE_URL)
                val con = url.openConnection() as HttpURLConnection
                con.doInput = false
                con.doOutput = true
                con.requestMethod = "POST"
                con.setRequestProperty("Content-Type", "application/json");

                // Put data in payload
                val out = OutputStreamWriter(con.outputStream);
                out.write("{\"authToken\":\"${user_token}\",\"deviceId\":\"${device_id}\"}");
                out.close()

                // Handle completed request
                if (con.responseCode == 200) {
                    // Go back to login screen
                    log("Usunięto urządzenie")
                    user_devices = user_devices.filter { id -> id != device_id }
                    runOnUiThread { device_adapter.removeDevice(device_id) }
                } else if (con.responseCode == 400) {
                    log("To urządzenie już nie jest powiązane z twoim kontem!")
                } else {
                    log("Wystąpił błąd podczas usuwania")
                }
            } catch (uhe: UnknownHostException) {
                log("Brak połączenia z internetem - nie można usunąć urządzenia")
            } catch (e: Exception) {
                log("Zwrócono wyjątek:\n${e}")
            }
            setButtons(true)
        }.start()
    }

    fun logOut() {
        Thread {
            try {
                setButtons(false)
                log("Wylogowywanie...")

                val url = URL(LOGOUT_URL)
                val con = url.openConnection() as HttpURLConnection
                con.doInput = false
                con.doOutput = true
                con.requestMethod = "POST"
                con.setRequestProperty("Content-Type", "application/json");

                // Put data in payload
                val out = OutputStreamWriter(con.outputStream);
                out.write("{\"authToken\":\"${user_token}\"}");
                out.close()

                // Handle completed request
                if (con.responseCode == 200) {
                    // Go back to login screen
                    log("Wylogowano pomyślnie")
                    finish()
                } else {
                    log("Wystąpił błąd podczas wylogowywania: ${con.responseCode}: ${con.responseMessage}")
                }
            } catch (uhe: UnknownHostException) {
                log("Brak połączenia z internetem - nie można się wylogować")
            } catch (e: Exception) {
                log("Zwrócono wyjątek:\n${e}")
            }
            setButtons(true)
        }.start()
    }

    private fun log(message: String) { runOnUiThread { textView_logHomepage.text = message } }
    private fun logAppend(message: String) { log(textView_logHomepage.text.toString() + "\n" + message) }

    fun setButtons(enabled: Boolean, refreshOnly: Boolean = false) {
        runOnUiThread {
            button_refresh.isClickable = enabled
            button_refresh.isEnabled = enabled
            if (refreshOnly) return@runOnUiThread

            button_pair.isClickable = enabled
            button_pair.isEnabled = enabled
            button_logout.isClickable = enabled
            button_logout.isEnabled = enabled
        }
    }
}