using System;
 
 namespace Company.Function
 {
    public class TelemetryData
    {
        public string timestamp { get; set; }
        public string device_id { get; set; }
        public string temperature { get; set; }
        //public DateTime date { get; set; } 
        public TelemetryData(string time1, string deviceId1, string temp1)
        {
            this.timestamp = time1;
            this.device_id = deviceId1;
            this.temperature = temp1;
            //this.date = new DateTime(temp1);
        }

        public static string MakeJson(string timestamp, string deviceId, string temp)
        {
            return "{\"timestamp\":\"" + timestamp + "\",\n\"device_id\":\"" + deviceId +"\", \n\"device_id\":\"" + temp +"\"\n}";
        }
    }

 }