using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Azure.Data.Tables;
using Azure;

namespace Company.Function
{
    public static class FindUser
    {
        public static UserTable Find(
            string login,
            string password,
            TableClient usersTable,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function finding user triggered.");

            bool ok = true;

            if(login == null)
            {
                log.LogInformation("login on null.");
                ok = false;
            }
            if(password == null)
            {
                log.LogInformation("password on null.");
                ok = false;
            }

            if(!ok) { return null; }
            
            var enumerator = usersTable.Query<UserTable>(filter: $"Login eq '{login}'").GetEnumerator();
            if(enumerator.MoveNext())
            {
                return enumerator.Current;
            }
            return null;
        }
    }
}
