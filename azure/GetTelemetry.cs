using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Azure.Data.Tables;
using System.Collections.Generic;
using Azure.Storage.Blobs;
using Microsoft.Azure.Cosmos;
using Microsoft.Azure.Documents;

namespace Company.Function
{
    public static class GetTelemetry
    {
        [FunctionName("GetTelemetry")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            [Table("users", Connection = "iotc8ca7_STORAGE")] TableClient usersTable,
            [Table("devices", Connection = "iotc8ca7_STORAGE")] TableClient devicesTable, 
            /*[CosmosDB(
            databaseName: "ToDoList",
            collectionName: "Items",
            //CreateLeaseCollectionIfNotExists = true,
            ConnectionStringSetting = "COSMOS_DB"),
            ]CosmosClient client,*/
            ILogger log)
        {
            log.LogDebug("Get Telemetry - begin");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string auth = data.authToken;

            if(auth == null)
            {
                log.LogInformation("GetTelemetry - authToken on null.");
                return new BadRequestObjectResult("Wymagany sessionToken");
            }

            UserTable user = Login.getUserBySessionToken(auth, usersTable);
            if(user == null)
            {
                log.LogInformation("GetTelemetry - user query by SessionToken on null.");
                return new BadRequestObjectResult("Nie znaleziono takiej sesji");
            }
            
            var devicesDb = devicesTable.Query<DeviceTable>(filter: $"RowKeyUser eq '{user.RowKey}'");

            HashSet<string> allowedDevicedIds = new HashSet<string>();
            foreach(var device in devicesDb)
            {
                allowedDevicedIds.Add(device.DeviceId); 
            }

            log.LogInformation("GetTelemetry - got devices Id.");

            Login.updateLoginSession(usersTable, user);
            log.LogInformation("Get Telemetry - session updated.");

            string content = "";
            string lackTelemetry = "- brak pomiaru -";
            Dictionary<string, TelemetryData> lastTelemetry = new Dictionary<string, TelemetryData>();
            foreach(string devId in allowedDevicedIds)
            {
                lastTelemetry.Add(devId, new TelemetryData(lackTelemetry, devId, "--,--"));
            }

            try
            {
                bool blobExist = false;
                int minutesBack = 0;
                int limit = 10;

                BlobClient blobClient;
                do
                {
                    //++minutesBack;
                    minutesBack += 1;
                    DateTime now = DateTime.UtcNow.Subtract(new System.TimeSpan(0, 0, minutesBack, 0));

                    string month = GetTelemetry.numberToStr(now.Month);
                    string day = GetTelemetry.numberToStr(now.Day);
                    string hour = GetTelemetry.numberToStr(now.Hour);
                    string minute = GetTelemetry.numberToStr(now.Minute);

                    string path = $"/Nasz-iot-hub-1/01/{now.Year}/{month}/{day}/{hour}/{minute}.json";

                    log.LogError("path: " + path);

                    BlobServiceClient blob = new BlobServiceClient("DefaultEndpointsProtocol=https;AccountName=iotc8ca7;AccountKey=qtDWNwlQuXKERHvaszkHDaA8KhlPRnbqtn01vpAJcbyFhu+RrOFgMGfLse/0LmaTuP9ZVIEOOJqk+AStIfR7Bg==;EndpointSuffix=core.windows.net");
                    BlobContainerClient container = blob.GetBlobContainerClient("telemetry-data");
                    blobClient = container.GetBlobClient(path);

                    blobExist = container.Exists() && blobClient.Exists();

                }while(!blobExist && limit > minutesBack);

                if(!blobExist)
                {
                    return new OkObjectResult(JsonConvert.SerializeObject(lastTelemetry.Values));
                }

                MemoryStream memoryStream = new MemoryStream();

                blobClient.DownloadTo(memoryStream);
                memoryStream.Position = 0;

                content = new StreamReader(memoryStream).ReadToEnd();
            }
            catch(Exception e)
            {
                log.LogError(e.Message);
                return new OkObjectResult(JsonConvert.SerializeObject(lastTelemetry.Values));
            }

            //List<TelemetryData> dataToSend = new List<TelemetryData>();

           /* foreach(var row in input)
            {
                var properties = row.GetPropertyValue<Document>("SystemProperties");
                string id = properties.GetPropertyValue<string>("iothub-connection-auth-generation-id");
                if(allowedDevicedIds.Contains(id))
                {
                    TelemetryData telemetryRow = new TelemetryData( "time", 
                                                                    id, 
                                                                    "pomiar");
                    dataToSend.Add(telemetryRow);
                }
            }*/

            dynamic telemetry = JsonConvert.DeserializeObject('[' + content.Replace('\n', ',') + ']');

            foreach(var json in telemetry)
            {
                //string deviceId = json.SystemProperties.connectionDeviceGenerationId.ToString();
                string jsonBody = System.Text.Encoding.UTF8.GetString(Convert.FromBase64String((string)json.Body));
                dynamic body = JsonConvert.DeserializeObject<TelemetryJsonBody>(jsonBody);

                string deviceId = body.mac.ToString();
                string temp = body.Temperature.ToString();

                if(allowedDevicedIds.Contains(deviceId))
                {
                    TelemetryData telemetryRow = new TelemetryData( json.EnqueuedTimeUtc.ToString(), 
                                                                    deviceId, 
                                                                    temp);

                    if(lastTelemetry[deviceId].timestamp == lackTelemetry)
                    {
                        lastTelemetry[deviceId] = telemetryRow;
                    }   
                    else
                    {
                        if(String.Compare(telemetryRow.timestamp, lastTelemetry[deviceId].timestamp) < 0)
                        {
                            lastTelemetry[deviceId] = telemetryRow;
                        }
                    }                                             
                    //dataToSend.Add(telemetryRow);
                }
            }
            
            return new OkObjectResult(JsonConvert.SerializeObject(lastTelemetry.Values));
        }

        private static string numberToStr(int number)
        {
            if(number > 9)
            {
                return number.ToString();
            }
            else if(number > 0)
            {
                return '0' + number.ToString();
            }
            else
            {
                return "00";
            }
        }
    }
}
