using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Azure.Data.Tables;
using Azure;

namespace Company.Function
{
    public static class Logout
    {
        [FunctionName("Logout")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            [Table("users", Connection = "iotc8ca7_STORAGE")] TableClient usersTable, 
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            //string auth = req.Query["authToken"];

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string auth = data.authToken;

            if(auth == null)
            {
                log.LogInformation("Logout - authToken on null.");
                return new BadRequestObjectResult("Wymagany sessionToken");
            }

            UserTable user = null;

            user = Login.getUserBySessionToken(auth, usersTable);

            if(user == null)
            {
                log.LogInformation("Logout - user query by SessionToken on null.");
                return new BadRequestObjectResult("Nie znaleziono takiej sesji");
            }

            user.SessionToken = "";

            usersTable.UpdateEntity<UserTable>(user, ETag.All, TableUpdateMode.Replace);

            return new OkObjectResult("Wylogowano pomyślnie");
        }
    }
}
