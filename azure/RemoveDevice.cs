using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Azure.Data.Tables;
using Azure;

namespace Company.Function
{
    public static class RemoveDevice
    {
        [FunctionName("RemoveDevice")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            [Table("users", Connection = "iotc8ca7_STORAGE")] TableClient usersTable, 
            [Table("devices", Connection = "iotc8ca7_STORAGE")] TableClient devicesTable, 
            ILogger log)
        {
            log.LogInformation("RemoveDevice - begin.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string auth = data.authToken;
            string deviceId = data.deviceId;

            if(auth == null)
            {
                log.LogInformation("RemoveDevice - authToken on null.");
                return new BadRequestObjectResult("Wymagany sessionToken");
            }
            if(deviceId == null)
            {
                log.LogInformation("RemoveDevice - deviceId on null.");
                return new BadRequestObjectResult("Wymagane deviceId");
            }

            UserTable user = Login.getUserBySessionToken(auth, usersTable);

            if(user == null)
            {
                log.LogInformation("RemoveDevice - user query by SessionToken on null.");
                return new BadRequestObjectResult("Nie znaleziono takiej sesji");
            }

            DeviceTable device = null;

            var enumerator = devicesTable.Query<DeviceTable>(filter: $"RowKeyUser eq '{user.RowKey}' and DeviceId eq '{deviceId}'").GetEnumerator();
            if(enumerator.MoveNext())
            {
                device = enumerator.Current;
            }

            if(device == null)
            {
                log.LogInformation("RemoveDevice - device from storage on null.");
                return new BadRequestObjectResult("Nie znaleziono takiego urządzenia");
            }

            var operation = devicesTable.DeleteEntity(device.PartitionKey, device.RowKey, ETag.All);

            if(operation.IsError)
            {
                log.LogInformation("Delete device failed.");
                return new BadRequestObjectResult("Usuwanie urządzenia nie powiodło się!");
            }

            Login.updateLoginSession(usersTable, user);

            return new OkObjectResult("Urządzenie zostało usunięte");
        }
    }
}
