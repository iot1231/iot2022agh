using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Azure;
using Azure.Data.Tables;

namespace Company.Function
{
    public static class Register
    {
        [FunctionName("Register")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "post", Route = "register")] HttpRequest req,
            [Table("users", Connection = "iotc8ca7_STORAGE")] TableClient usersTable,
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger register request.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            UserTable newUser = JsonConvert.DeserializeObject<UserTable>(requestBody);

            UserTable user = FindUser.Find(newUser.Login, newUser.Password, usersTable, log);

            if(user != null)
            {
                log.LogInformation("findUser found user with this login.");
                return new BadRequestObjectResult("Już istnieje taki użytkownik!");
            }

            var operation = await usersTable.AddEntityAsync<UserTable>(newUser);

            if(operation.IsError)
            {
                log.LogInformation("Adding new user failed.");
                return new BadRequestObjectResult("Dodawanie użytkownika nie powiodło się!");
            }

            return new OkObjectResult("Rejestracja przebiegła pomyślnie, możesz teraz się zalogować");
        }
    }
}
