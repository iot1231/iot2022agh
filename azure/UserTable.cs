using System;
using Azure;
 
 namespace Company.Function
 {
    public class UserTable : Azure.Data.Tables.ITableEntity
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public DateTimeOffset? Timestamp { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public ETag ETag { get; set; }
        public string SessionToken { get; set; }
        public DateTime ExpirationSessionToken { get; set; }

        public UserTable()
        {
            PartitionKey = "Http";
            RowKey = Guid.NewGuid().ToString();
            Timestamp = DateTime.UtcNow;
            ExpirationSessionToken = DateTime.UtcNow;
        }
    }

 }