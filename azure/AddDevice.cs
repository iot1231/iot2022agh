using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Azure.Data.Tables;

namespace Company.Function
{
    public static class AddDevice
    {
        [FunctionName("AddDevice")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Function, "get", "post", Route = null)] HttpRequest req,
            [Table("users", Connection = "iotc8ca7_STORAGE")] TableClient usersTable, 
            [Table("devices", Connection = "iotc8ca7_STORAGE")] TableClient devicesTable, 
            ILogger log)
        {
            log.LogInformation("AddDevice - begin.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            dynamic data = JsonConvert.DeserializeObject(requestBody);
            string auth = data.authToken;
            string deviceId = data.deviceId;

            if(auth == null)
            {
                log.LogInformation("AddDevice - authToken on null.");
                return new BadRequestObjectResult("Wymagany sessionToken");
            }
            if(deviceId == null)
            {
                log.LogInformation("AddDevice - deviceId on null.");
                return new BadRequestObjectResult("Wymagane deviceId");
            }

            UserTable user = Login.getUserBySessionToken(auth, usersTable);

            if(user == null)
            {
                log.LogInformation("AddDevice - user query by SessionToken on null.");
                return new BadRequestObjectResult("Nie znaleziono takiej sesji");
            }

            DeviceTable newDevice = new DeviceTable();
            newDevice.RowKeyUser = user.RowKey;
            newDevice.DeviceId = deviceId;

            DeviceTable existingDeviceOfUser = null;

            var enumeratorDevices = devicesTable.Query<DeviceTable>(filter: $"DeviceId eq '{deviceId}' and RowKeyUser eq '{user.RowKey}'").GetEnumerator();
            if(enumeratorDevices.MoveNext())
            {
                existingDeviceOfUser = enumeratorDevices.Current;
            }
            
            if(existingDeviceOfUser != null)
            {
                log.LogInformation("AddDevice - device already assigned to user.");
                return new BadRequestObjectResult("Już masz dodane to urządzenie");
            }

            var operation = await devicesTable.AddEntityAsync<DeviceTable>(newDevice);

            if(operation.IsError)
            {
                log.LogInformation("Adding new device failed.");
                return new BadRequestObjectResult("Dodawanie nowego urządzenia nie powiodło się!");
            }

            Login.updateLoginSession(usersTable, user);

            return new OkObjectResult("Urządzenie dodano pomyślnie");
        }
    }
}
