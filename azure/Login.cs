using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections;
using Azure.Data.Tables;
using Microsoft.WindowsAzure.Storage.Table;
using Azure;
using System.Linq;
using System.Collections.Generic;

namespace Company.Function
{
    public static class Login
    {
        [FunctionName("Login")]
        public static async Task<IActionResult> Run(
            [HttpTrigger(AuthorizationLevel.Anonymous, "post", Route = "user")] HttpRequest req, 
            [Table("users", Connection = "iotc8ca7_STORAGE")] TableClient usersTable, 
            [Table("devices", Connection = "iotc8ca7_STORAGE")] TableClient devicesTable, 
            ILogger log)
        {
            log.LogInformation("C# HTTP trigger function processed a request.");

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            UserTable data = JsonConvert.DeserializeObject<UserTable>(requestBody);

            //TODO: validate
            UserTable user = FindUser.Find(data.Login, data.Password, usersTable, log);

            log.LogInformation("Find User executed corretly");

            if(user == null)
            {
                log.LogInformation("findUser returned null.");
                return new BadRequestObjectResult("Nie znaleziono użytkownika o takim loginie");
            }

            if(data.Password == null || data.Password == "" || user.Password != data.Password)
            {
                log.LogInformation("Login - wrong password.");
                return new BadRequestObjectResult("Niepoprawne hasło!");
            }

            string authToken = Login.generateToken(32);

            DateTime expirationDateToken = generateExpirationDateTime(10);

            user.SessionToken = authToken;
            user.ExpirationSessionToken = expirationDateToken;

            usersTable.UpdateEntity<UserTable>(user, ETag.All, TableUpdateMode.Replace);

            //pobieranie id urzadzen uzytkownika

            var devicesDb = devicesTable.Query<DeviceTable>(filter: $"RowKeyUser eq '{user.RowKey}'");


            System.Text.StringBuilder response = new System.Text.StringBuilder();

            response.Append(authToken + ";");

            foreach(var device in devicesDb)
            {
                response.Append(device.DeviceId + ","); 
            }

            return (ActionResult)new OkObjectResult(response.ToString());
        }

        private static string generateToken(int Length)
        {
            Random random = new Random();

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, Length)
                .Select(s => s[random.Next(s.Length)]).ToArray());
        }

        private static DateTime generateExpirationDateTime(int minutes)
        {
            return DateTime.UtcNow.Add(new System.TimeSpan(0, 0, minutes, 0));
        }

        public static void updateLoginSession(TableClient usersTable, UserTable user)
        {
            if(user.SessionToken == "")
            {
                user.SessionToken = Login.generateToken(32);
            }
            if(DateTime.Compare(user.ExpirationSessionToken, DateTime.UtcNow) < 0)
            {
                user.ExpirationSessionToken = generateExpirationDateTime(10);
            }
            usersTable.UpdateEntity<UserTable>(user, ETag.All, TableUpdateMode.Replace);
        }

        public static UserTable getUserBySessionToken(string token, TableClient usersTable)
        {
            var enumerator = usersTable.Query<UserTable>(filter: $"SessionToken eq '{token}' and SessionToken ne ''").GetEnumerator();
            if(enumerator.MoveNext())
            {
                return enumerator.Current;
            }
            return null;
        }
    }
}
