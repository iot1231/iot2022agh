using System;
using Azure;
 
 namespace Company.Function
 {
    public class DeviceTable : Azure.Data.Tables.ITableEntity
    {
        public string PartitionKey { get; set; }
        public string RowKey { get; set; }
        public DateTimeOffset? Timestamp { get; set; }
        public ETag ETag { get; set; }
        public string RowKeyUser { get; set; }
        public string DeviceId { get; set; }

        public DeviceTable()
        {
            PartitionKey = "Http";
            RowKey = Guid.NewGuid().ToString();
        }
    }

 }